class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.string :detail
      t.decimal :amount

      t.timestamps
    end
  end
end
